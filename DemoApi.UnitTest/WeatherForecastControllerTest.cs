using DemoApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using Xunit;

namespace DemoApi.Test
{
    public class WeatherForecastControllerTest
    {
        private readonly WeatherForecastController _controller;
        readonly ILogger<WeatherForecastController> logger ;


        public WeatherForecastControllerTest()
        {
            _controller = new WeatherForecastController(logger);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.Get();
            // Assert
            Assert.IsType<WeatherForecast[]>(okResult as WeatherForecast[]);
        }

        [Fact]
        public void Get_WhenCalled_Failure()
        {
            var okResult = _controller.Get();
            // Assert
            Assert.IsType<WeatherForecast[]>(okResult as WeatherForecast[]);
        }
    }
}
